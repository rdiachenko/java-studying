package com.rusya7;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.TableItem;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SimpleGuiController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	private final SimpleGui simpleGui;

	public SimpleGuiController(SimpleGui simpleGui) {
		this.simpleGui = simpleGui;
		this.simpleGui.addStartServerListener(new StartServerListener());
	}

	private class StartServerListener extends SelectionAdapter {
		@Override
		public void widgetSelected(final SelectionEvent e) {
//			EventQueue.invokeLater(new Runnable() {
//				@Override
//				public void run() {
//					addItemsToTable();
//				}
//			});

			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					addItemsToTable();
				}
			});
		}
	}

	private void addItemsToTable() {
		TableViewer tableViewer = simpleGui.getTableViewer();

		for (int i = 0; i < 5; i++) {
			TableItem tableItem = new TableItem(tableViewer.getTable(), SWT.NONE);
			String itemText = "Item #" + i + " was created.";
			tableItem.setText(itemText);
			logger.info(itemText);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				logger.error("Current thread has been interrupted by another one: {}", e);
			}
		}
	}
}
