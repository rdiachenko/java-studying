package com.rusya7;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class SimpleGui {

	private static final String COLUMN_NAME = "Table Items";

	private static final String START_SERVER_BUTTON_NAME = "Start Server";

	private static final int COLUMN_WIDTH = 300;

	private TableViewer tableViewer;

	private Button startServerButton;

	public void buildGui(Shell shell) {
		buildServerButton(shell);
		buildTableViewer(shell);
	}

	public TableViewer getTableViewer() {
		return tableViewer;
	}

	private void buildServerButton(Shell shell) {
		startServerButton = new Button(shell, SWT.PUSH);
		startServerButton.setText(START_SERVER_BUTTON_NAME);
	}

	private void buildTableViewer(final Shell shell) {
		tableViewer = new TableViewer(shell, SWT.SINGLE | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER
				| SWT.FULL_SELECTION);

		Table dataTable = tableViewer.getTable();
		dataTable.setHeaderVisible(true);
		dataTable.setLinesVisible(true);
		GridData gridData = new GridData(SWT.FILL, SWT.FILL, true, true, 1, 50);
		dataTable.setLayoutData(gridData);

		createColumn();
	}

	private void createColumn() {
		TableViewerColumn viewerColumn = new TableViewerColumn(tableViewer, SWT.NULL);
		TableColumn column = viewerColumn.getColumn();
		column.setText(COLUMN_NAME);
		column.setWidth(COLUMN_WIDTH);
	}

	public void addStartServerListener(SelectionListener listener) {
		startServerButton.addSelectionListener(listener);
	}
}
