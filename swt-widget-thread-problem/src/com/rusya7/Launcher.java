package com.rusya7;

import org.apache.log4j.PropertyConfigurator;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Launcher {

	public static void main(String[] args) {
		Launcher launcher = new Launcher();
		PropertyConfigurator.configure("config/log4j.properties");
		launcher.logger.info("Log4j configuration was successfully loaded.");

		Display display = new Display();
		Shell shell = new Shell(display);
		shell.setLayout(new GridLayout());

		SimpleGui simpleGui = new SimpleGui();
		simpleGui.buildGui(shell);
		new SimpleGuiController(simpleGui);

		shell.pack();
		shell.setText("Simple Gui");
		shell.open();

		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
		display.dispose();
	}

	private final Logger logger = LoggerFactory.getLogger(getClass());
}
